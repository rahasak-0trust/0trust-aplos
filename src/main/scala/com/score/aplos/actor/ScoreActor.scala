package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.ScoreActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._
import scala.util.control.Breaks._

object ScoreActor {

  case class AddScore(messageType: String, execer: String, id: String, digsig: String,
                      userId: String, score: Double, denominator: Double) extends ScoreMessage

  case class UpdateScore(messageType: String, execer: String, id: String, digsig: String,
                         userId: String, score: Double, denominator: Double) extends ScoreMessage

  case class GetScore(messageType: String, execer: String, id: String, digsig: String,
                      userId: String) extends ScoreMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new ScoreActor())

}

class ScoreActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with FeatureToggleConf with AppLogger {

  override def receive: Receive = {
    case msg: AddScore =>
      logger.info(s"got AddScore message - $msg")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(msg.digsig, msg.toString)
      }

      val id = s"${msg.execer};AddScore;${msg.id}"
      if (!CassandraStore.isDoubleSpend(msg.execer, "AccountActor", msg.id) && RedisStore.set(id)) {
        implicit val format: JsonFormat[AddScore] = jsonFormat7(AddScore)
        val trans = Trans(msg.id, msg.execer, "ScoreActor", "AddScore", msg.toJson.toString, msg.digsig)
        CassandraStore.createTrans(trans)

        val score = Score(msg.userId, msg.score, msg.denominator)
        CassandraStore.createScore(score)

        sender ! StatusReply(200, "score added")
      } else {
        logger.error(s"double spend AddScore $msg")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case msgs: List[AddScore] =>
      logger.info(s"got List[AddScore] messages - $msgs")

      msgs.foreach{ msg =>
        // verify digital signature
        if (enableVerifySignature) {
          CryptoFactory.verifySignature(msg.digsig, msg.toString)
        }

        val id = s"${msg.execer};AddScore;${msg.id}"
        if (!CassandraStore.isDoubleSpend(msg.execer, "AddScore", msg.id) && RedisStore.set(id)) {
          implicit val format: JsonFormat[AddScore] = jsonFormat7(AddScore)
          val trans = Trans(msg.id, msg.execer, "ScoreActor", "AddScore", msg.toJson.toString, msg.digsig)
          CassandraStore.createTrans(trans)

          val score = Score(msg.userId, msg.score, msg.denominator)
          CassandraStore.createScore(score)
        } else {
          logger.error(s"double spend AddScore $msg")
          sender ! StatusReply(400, "double spend")
          break()
        }
      }

      sender ! StatusReply(200, "scores added")

      context.stop(self)

    case msg: GetScore =>
      logger.info(s"got GetScore message - $msg")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(msg.digsig, msg.toString)
      }

      CassandraStore.getScore(msg.userId) match {
        case Some(s) =>
          logger.info(s"found score $s")
          sender ! ScoreReply(s.id, s.score, s.denominator, DateFactory.formatToString(Option(s.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE))
        case None =>
          logger.error(s"score not found for ${msg.userId}")
          sender ! StatusReply(404, "not found")
      }

      context.stop(self)
  }

}
