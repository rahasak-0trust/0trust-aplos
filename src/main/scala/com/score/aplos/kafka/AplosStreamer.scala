package com.score.aplos.kafka

import akka.NotUsed
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableMessage
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.pattern.ask
import akka.stream.scaladsl.{Flow, GraphDSL, Partition, RunnableGraph, Sink}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, ClosedShape, Supervision}
import akka.util.Timeout
import com.score.aplos.actor.{IdentityActor, ScoreActor}
import com.score.aplos.config.{AppConf, FeatureToggleConf, KafkaConf}
import com.score.aplos.protocol.{IdentityMessage, ScoreMessage, StatusReply}
import com.score.aplos.util.AppLogger
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import spray.json._

import scala.concurrent.duration._

object AplosStreamer extends AppConf with KafkaConf with FeatureToggleConf with AppLogger {

  def server()(implicit system: ActorSystem): Unit = {
    // supervision
    // meterializer for streams
    val decider: Supervision.Decider = { e =>
      logError(e)
      Supervision.Resume
    }

    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    implicit val timeout = Timeout(90.seconds)

    // parse json
    import com.score.aplos.protocol.IdentityMessageProtocol._
    import com.score.aplos.protocol.ScoreMessageProtocol._
    import com.score.aplos.protocol.StatusReplyProtocol._

    // kafka consumer source
    val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(kafkaAddr)
      .withGroupId(kafkaGroup)
    val source = Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaTopic))

    // kafka producer sink
    val producerSettings = ProducerSettings(system, new StringSerializer, new StringSerializer)
      .withBootstrapServers(kafkaAddr)
    val kafkaSink = Producer.plainSink(producerSettings)

    // flow to map kafka message to IdentityMessage
    val toIdentityMessageFlow: Flow[CommittableMessage[String, String], IdentityMessage, NotUsed] = Flow[CommittableMessage[String, String]]
      .map { p =>
        logger.info(s"Got message ${p.record.value()}")
        p.record.value().parseJson.convertTo[IdentityMessage]
      }

    // flow to map kafka message to QualificationMessage
    val toQualificationMessageFlow: Flow[CommittableMessage[String, String], ScoreMessage, NotUsed] = Flow[CommittableMessage[String, String]]
      .map { p =>
        logger.info(s"Got message ${p.record.value()}")
        p.record.value().parseJson.convertTo[ScoreMessage]
      }

    // flow to handle IdentityMessage
    val handleIdentityMessageFlow: Flow[IdentityMessage, StatusReply, NotUsed] = Flow[IdentityMessage]
      .mapAsync(10) { msg =>
        (system.actorOf(IdentityActor.props()) ? msg)
          .mapTo[StatusReply]
      }

    // flow to handle IdentityMessage
    val handleQualificationMessageFlow: Flow[ScoreMessage, StatusReply, NotUsed] = Flow[ScoreMessage]
      .mapAsync(10) { msg =>
        (system.actorOf(ScoreActor.props()) ? msg)
          .mapTo[StatusReply]
      }

    // flow to publish status to registrar-api
    val toGatewayProducerRecordFlow = Flow[StatusReply]
      .map(p => new ProducerRecord[String, String](kafkaGatewayTopic, p.toJson.toString))

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val partitioner = (msg: CommittableMessage[String, String]) => {
        if (msg.record.value().contains("AccountActor")) 0
        else if (msg.record.value().contains("QualificationActor")) 1
        else 2
      }

      val partition = b.add(Partition[CommittableMessage[String, String]](3, partitioner))

      // partition stream in three flows
      //  1. IdentityMessage
      //  2. QualificationMessage
      //  3. UnsupportedMessage(ignore sink)
      source ~> partition
      partition.out(0) ~> toIdentityMessageFlow ~> handleIdentityMessageFlow ~> toGatewayProducerRecordFlow ~> kafkaSink
      partition.out(1) ~> toQualificationMessageFlow ~> handleQualificationMessageFlow ~> toGatewayProducerRecordFlow ~> kafkaSink
      partition.out(2) ~> Sink.ignore

      ClosedShape
    })
    graph.run()
  }

}

