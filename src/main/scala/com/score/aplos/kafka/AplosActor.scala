package com.score.aplos.kafka

import akka.actor.{Actor, ActorSystem, Props}
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.score.aplos.actor.IdentityActor
import com.score.aplos.config.{FeatureToggleConf, KafkaConf}
import com.score.aplos.protocol.IdentityMessage
import com.score.aplos.util.{AppLogger, CryptoFactory}
import org.apache.kafka.common.serialization.StringDeserializer
import spray.json._

object AplosActor {

  case class Stream()

  def props()(implicit system: ActorSystem) = Props(new AplosActor)

}

class AplosActor()(implicit system: ActorSystem) extends Actor with AppLogger with KafkaConf with FeatureToggleConf {

  override def receive: Receive = {
    case Stream =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      // kafka source
      val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaAddr)
        .withGroupId(kafkaGroup)
      val source = Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaTopic))

      // consumer as stream
      source
        .map(kmsg => {
          // got the message
          val msg = kmsg.record.value
          logger.info(s"got actor message - $msg")

          msg match {
            case _ if msg.contains("AccountActor") =>
              // for identity actor
              logger.info(s"msg for  actor - $msg")

              import com.score.aplos.protocol.IdentityMessageProtocol._
              val actorMsg = msg.parseJson.convertTo[IdentityMessage]

              // verify digital signature if feature toggle enabled
              if (enableVerifySignature) {
                CryptoFactory.verifySignature("digsig", actorMsg.toString)
              }
              context.actorOf(IdentityActor.props()) ! actorMsg

            case _ if msg.contains("QualificationActor") =>
              // for qualification actor
              logger.info(s"msg for qualification actor - $msg")

              import com.score.aplos.protocol.IdentityMessageProtocol._
              val actorMsg = msg.parseJson.convertTo[IdentityMessage]

              // verify digital signature if feature toggle enabled
              if (enableVerifySignature) {
                CryptoFactory.verifySignature("digsig", actorMsg.toString)
              }
              context.actorOf(IdentityActor.props()) ! actorMsg
            case _ =>
              // for other actor
              logger.info(s"msg for unknown actor - $msg")
          }
        })
        .runWith(Sink.ignore)
  }

}

