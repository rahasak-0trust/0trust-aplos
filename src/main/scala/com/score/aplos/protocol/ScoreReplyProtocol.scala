package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class ScoreReply(userId: String,
                      numerator: Double,
                      denominator: Double,
                      timestamp: Option[String]
                     )

object ScoreReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val format = jsonFormat4(ScoreReply)

}

