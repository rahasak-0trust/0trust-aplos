package com.score.aplos.protocol

import com.score.aplos.cassandra.{AlResult, OlResult}
import spray.json.{DefaultJsonProtocol, JsObject, JsValue, JsonFormat, RootJsonFormat, _}

case class QualificationMeta(offset: String, limit: String, count: String, total: String)

case class QualificationReply(id: String,
                              holder: String,

                              olResult: Option[OlResult] = None,
                              alResult: Option[AlResult] = None,

                              hasDiploma: Option[Boolean] = None,
                              nameDiploma: Option[String] = None,
                              hasExperience: Option[Boolean] = None,
                              nameExperience: Option[String] = None,
                              durationExperience: Option[String] = None,

                              professionArea: Option[String] = None,
                              professionName: Option[String] = None,

                              status: Option[String] = None,

                              timestamp: Option[String])

case class QualificationSearchReply(meta: QualificationMeta, projects: List[QualificationReply])

object QualificationSearchReplyProtocol extends DefaultJsonProtocol {
  implicit val f1: JsonFormat[OlResult] = jsonFormat10(OlResult)
  implicit val f2: JsonFormat[AlResult] = jsonFormat4(AlResult)
  implicit val f3: JsonFormat[QualificationMeta] = jsonFormat4(QualificationMeta)
  implicit val f4: JsonFormat[QualificationReply] = jsonFormat13(QualificationReply)

  implicit object QualificationSearchReplyFormat extends RootJsonFormat[QualificationSearchReply] {
    override def write(obj: QualificationSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("qualifications", obj.projects.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}
