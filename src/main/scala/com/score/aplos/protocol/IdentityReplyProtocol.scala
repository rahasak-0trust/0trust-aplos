package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class IdentityReply(did: String, owner: String,
                         roles: List[String], groups: List[String], pubKey: Option[String],
                         nic: Option[String],
                         name: Option[String],
                         dob: Option[String],
                         phone: Option[String],
                         email: Option[String],
                         address: Option[String],
                         blobId: Option[String],
                         blob: Option[String],
                         activated: Boolean, disabled: Boolean, timestamp: Option[String])

object IdentityReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val format = jsonFormat16(IdentityReply)

}

