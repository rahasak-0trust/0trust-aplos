package com.score.aplos.protocol

import com.score.aplos.actor.ScoreActor._
import spray.json._

trait ScoreMessage

object ScoreMessageProtocol extends DefaultJsonProtocol {

  implicit val format1: JsonFormat[AddScore] = jsonFormat7(AddScore)
  implicit val format2: JsonFormat[GetScore] = jsonFormat5(GetScore)

  implicit object ScoreMessageFormat extends RootJsonFormat[ScoreMessage] {
    def write(obj: ScoreMessage): JsValue =
      JsObject((obj match {
        case p: AddScore => p.toJson
        case p: GetScore => p.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): ScoreMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("addScore")) => json.convertTo[AddScore]
        case Seq(JsString("getScore")) => json.convertTo[GetScore]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}


