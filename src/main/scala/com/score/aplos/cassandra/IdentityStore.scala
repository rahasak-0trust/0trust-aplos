package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import com.datastax.driver.core.LocalDate

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}

case class Location(lat: Double, lon: Double)

case class Device(deviceToken: Option[String],
                  deviceType: Option[String],
                  deviceImei: Option[String])

case class Answer(answer1: Option[String],
                  answer2: Option[String],
                  answer3: Option[String])

case class Address(address: Option[String],
                   province: Option[String],
                   district: Option[String],
                   dsDivision: Option[String],
                   gsDivision: Option[String],
                   lat: Option[Double],
                   lon: Option[Double])

case class Identity(did: String, owner: String,
                    password: String,
                    roles: Set[String],
                    groups: Set[String],
                    pubKey: Option[String],

                    answer1: Option[String] = None,
                    answer2: Option[String] = None,
                    answer3: Option[String] = None,

                    name: Option[String] = None,
                    nic: Option[String] = None,
                    gender: Option[String] = None,
                    age: Option[Int] = None,
                    dob: Option[LocalDate] = None,
                    phone: Option[String] = None,
                    email: Option[String] = None,
                    taxId: Option[String] = None,
                    employer: Option[String] = None,
                    blobId: Option[String] = None,

                    address: Option[String] = None,
                    province: Option[String] = None,
                    district: Option[String] = None,
                    dsDivision: Option[String] = None,
                    gsDivision: Option[String] = None,
                    location: Option[Location] = None,

                    deviceToken: Option[String] = None,
                    deviceType: Option[String] = None,
                    deviceImei: Option[String] = None,

                    salt: String,
                    attempts: Int,
                    activated: Boolean,
                    disabled: Boolean,
                    timestamp: Date = new Date)

trait IdentityStore extends CassandraCluster {

  import ctx._

  def createIdentityAsync(identity: Identity): Future[Unit] = {
    val q = quote {
      query[Identity].insert(lift(identity))
    }
    ctx.run(q)
  }

  def updateIdentityAsync(identity: Identity): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(identity.did) && p.owner == lift(identity.owner))
        .update(
          _.name -> lift(identity.name),
          _.phone -> lift(identity.phone),
          _.email -> lift(identity.email),
          _.address -> lift(identity.address)
        )
    }
    ctx.run(q)
  }

  def registerIdentityAsync(identity: Identity): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(identity.did) && p.owner == lift(identity.owner))
        .update(
          _.password -> lift(identity.password),
          _.deviceType -> lift(identity.deviceType),
          _.deviceToken -> lift(identity.deviceToken),
          _.deviceImei -> lift(identity.deviceImei),
          _.answer1 -> lift(identity.answer1),
          _.answer2 -> lift(identity.answer2),
          _.answer3 -> lift(identity.answer3),
          _.activated -> lift(true)
        )
    }
    ctx.run(q)
  }

  def updateDeviceAsync(did: String, owner: String, device: Device): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.deviceImei -> lift(device.deviceImei),
          _.deviceType -> lift(device.deviceType),
          _.deviceToken -> lift(device.deviceToken)
        )
    }
    ctx.run(q)
  }

  def updateAnswerAsync(did: String, owner: String, answer: Answer): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.answer1 -> lift(answer.answer1),
          _.answer2 -> lift(answer.answer2),
          _.answer3 -> lift(answer.answer3)
        )
    }
    ctx.run(q)
  }

  def updateRolesAsync(did: String, owner: String, roles: Set[String]): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.roles -> lift(roles)
        )
    }
    ctx.run(q)
  }

  def updatePasswordAsync(did: String, owner: String, password: String): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.password -> lift(password)
        )
    }
    ctx.run(q)
  }

  def updateSaltAsync(did: String, owner: String, salt: String): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.salt -> lift(salt)
        )
    }
    ctx.run(q)
  }

  def updateAttemptsAsync(did: String, owner: String, attempts: Int): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.attempts -> lift(attempts)
        )
    }
    ctx.run(q)
  }

  def activateIdentityAsync(did: String, owner: String, activated: Boolean): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.activated -> lift(activated)
        )
    }
    ctx.run(q)
  }

  def disableIdentityAsync(did: String, owner: String, disabled: Boolean): Future[Unit] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .update(
          _.disabled -> lift(disabled)
        )
    }
    ctx.run(q)
  }

  def searchIdentityAsync(did: String, owner: String): Future[List[Identity]] = {
    val q = quote {
      query[Identity]
        .filter(p => p.did == lift(did) && p.owner == lift(owner))
        .take(1)
    }
    ctx.run(q)
  }

}

import scala.concurrent.duration._

//object M2 extends App with AccountStore {
//  // create account
//  val a1 = Account(
//    "111", "sampath", "111", "agent", Set(), "", "", "", "", "", "", "", "111", "111", "12/12/21", "0221", "e@g.com", "ops", "111", "", 1, false, false
//  )
//
//  //Await.result(createAccountAsync(a1), 10.seconds)
//  Await.result(updateRolesAsync("111", "sampath", Set("kandy", "ops")), 10.seconds)
//}

