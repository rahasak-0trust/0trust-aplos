package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}

case class Score(id: String, score: Double, denominator: Double, timestamp: Date = new Date())

trait ScoreStore extends CassandraCluster {
  import ctx._

  def createScoreAsync(score: Score): Future[Unit] = {
    val q = quote {
      query[Score].insert(lift(score))
    }
    ctx.run(q)
  }

  def updateScoreAsync(score: Score): Future[Unit] = {
    val q = quote {
      query[Score]
        .filter(p => p.id == lift(score.id))
        .update(
          _.score -> lift(score.score),
          _.denominator -> lift(score.denominator)
        )
    }
    ctx.run(q)
  }

  def getScoreAsync(id: String): Future[List[Score]] = {
    val q = quote {
      query[Score]
        .filter(p => p.id == lift(id))
        .take(1)
    }
    ctx.run(q)
  }
}
