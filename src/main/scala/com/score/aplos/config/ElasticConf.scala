package com.score.aplos.config

import com.typesafe.config.ConfigFactory

trait ElasticConf {
  val elasticConf = ConfigFactory.load("elastic.conf")

  lazy val elasticCluster = elasticConf.getString("elastic.cluster")

  lazy val transElasticIndex = elasticConf.getString("elastic.trans-index")
  lazy val transElasticDocType = elasticConf.getString("elastic.trans-doc-type")
  lazy val identityElasticIndex = elasticConf.getString("elastic.identity-index")
  lazy val identityElasticDocType = elasticConf.getString("elastic.identity-doc-type")
  lazy val scoreElasticIndex = elasticConf.getString("elastic.score-index")
  lazy val scoreElasticDocType = elasticConf.getString("elastic.score-doc-type")

  lazy val elasticHosts = elasticConf.getString("elastic.hosts").split(",").toSeq
  lazy val elasticPort = elasticConf.getString("elastic.port").toInt
}