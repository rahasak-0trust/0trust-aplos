package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait ApiConf {

  // config object
  val apiConf = ConfigFactory.load("api.conf")

  // senzie config
  lazy val notificationApi = Try(apiConf.getString("api.notification")).getOrElse("http://dev.localhost:8762/api/notifications")

}
